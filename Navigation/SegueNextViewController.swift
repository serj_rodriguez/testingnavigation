//
//  SegueNextViewController.swift
//  Navigation
//
//  Created by Sergio Andres Rodriguez Castillo on 14/12/23.
//

import UIKit

class SegueNextViewController: UIViewController {
    var labelText: String?
    
    @IBOutlet private(set) var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = labelText
    }
    
    deinit {
        print(">> SegueNextViewController.deinit")
    }
}
